const fs = require('fs');
const express = require('express')
const app = express()

app.use(express.static('public'));
app.use(express.static('pages'));

app.get('/*', function (req, res) {
    try {
        const html = fs.readFileSync('index.html', 'utf-8');
        let page = req.url;
        if (page.endsWith('/')) {
            page = page.substring(0, page.length - 1);
        }
        console.log(page);

        const pageHtml = fs.readFileSync(`pages${page}${page}.html`, 'utf-8');

        const newHtml = html.replace(/{{content}}/, pageHtml);
        res.send(newHtml);
    } catch (error) {
        res.send('no page');
    }
})

app.listen(3002, () => {
    console.log('express server is up and running on port 3002');
})