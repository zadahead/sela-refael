

const LOGIN_KEY = 'user';

const isLogin = () => {
    return localStorage.getItem(LOGIN_KEY);
}

const setLogin = (user) => {
    localStorage.setItem(LOGIN_KEY, user);
}

const logout = () => {
    localStorage.removeItem(LOGIN_KEY);
}