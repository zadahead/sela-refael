const base = {
    nav: [
        {
            href: '/home',
            text: 'Home'
        },
        {
            href: '/box',
            text: 'Box'
        },
        {
            href: '/users',
            text: 'Users'
        }
    ],
    options: [
        ['option1.html', '<i class="fal fa-users"></i>'],
        ['option2.html', '<i class="fas fa-user"></i>'],
        ['option3.html', '<i class="fas fa-heart"></i>']
    ]
}

function drawPage() {
    //grid
    const div = document.createElement('div');
    div.classList.add('grid');

    //header
    const header = createHeader();

    //content
    const content = document.getElementById('content');

    //append
    div.append(header);
    div.append(content);

    //append to body
    document.body.append(div);
}

function createHeader() {
    const wrapper = document.createElement('div');

    //between
    const between = document.createElement('div');
    between.classList.add('line');
    between.classList.add('between');

    //lineLeft
    const lineLeft = document.createElement('div');
    lineLeft.classList.add('line');

    base.nav.forEach(item => {
        lineLeft.append(createA(item.href, item.text))
    });

    //lineRight
    const lineRight = document.createElement('div');
    lineRight.classList.add('line');

    base.options.forEach(item => {
        lineRight.append(createA(item[0], item[1]));
    })

    handleAuth(lineRight);


    //append to between
    between.append(lineLeft);
    between.append(lineRight);

    //append to wrapper
    wrapper.append(between);

    return wrapper;
}

function createA(href, text) {
    const a = document.createElement('a');
    a.innerHTML = text;
    a.href = href;

    return a;
}

const handleAuth = (lineRight) => {
    const publicUrls = ['/login/']

    const user = isLogin();
    if (user) {
        const userElem = document.createElement('div');
        userElem.innerText = user;
        lineRight.append(userElem);
        lineRight.append(createA('/logout', 'Logout'))
    } else {
        const location = window.location.pathname;
        if (!publicUrls.includes(location)) {
            window.location.href = '/login';
        }
    }
}

drawPage();