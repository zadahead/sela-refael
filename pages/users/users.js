class Users {
    constructor(list) {
        this.list = list;
        this.#init();
    }

    #init = () => {
        this.list.forEach(this.#add);

        this.#attachAdd();
    }

    #append = (user) => {
        document.getElementById('usersList').append(user.div);
    }

    #attachAdd = () => {
        const btn = document.querySelector('.v_users button');
        btn.addEventListener('click', this.handleAdd);
    }

    #add = (user) => {
        const u = new User(user);
        u.attach(this.handleClick);
        this.#append(u);
    }

    handleAdd = () => {
        console.log('handleAdd');
        this.#add({
            name: 'Mosh'
        })
    }

    handleClick = (user) => {
        console.log('handleClick');
        console.log('user', user);
        console.log('this', this);
        const isOk = confirm("This will remove the user, continue?");
        if (isOk) {
            user.div.remove();
        }
    }
}