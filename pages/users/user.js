class User {
    constructor(user) {
        this.user = user;
        this.#init();
    }

    #init = () => {
        this.#create();
    }

    #create = () => {
        const div = document.createElement('div');
        div.classList.add('user');

        div.innerHTML = this.user.name;

        this.div = div;
    }

    attach = (onclick) => {
        this.onclick = onclick;
        this.div.addEventListener('click', this.handleClick);
    }

    handleClick = () => {
        this.onclick({
            user: this.user,
            div: this.div
        });
    }




}