console.log('Users page loaded');







const fetchUrl = (callback) => {
    console.log('fetching...');
    setTimeout(() => {
        callback();
    }, 3000);
}

const fetchPromise = () => {
    return new Promise((res, rej) => {
        setTimeout(() => {
            try {
                res(15);
            } catch (error) {
                rej(error);
            }
        }, 2000);
    });
}


// fetchPromise()
//     .then((resp) => {
//         console.log('done.', resp);
//         return resp + 20;
//     })
//     .catch((error) => {
//         console.log('error.', error)
//     });





// fetch('https://jsonplaceholder.typicode.com/todos')
//     .then((response) => {
//         return response.json()
//     })
//     .then((json) => {
//         console.log(json)
//     })
//     .catch((error) => {
//         console.log('errrrr', error);
//     })



const fetchAwait = async () => {
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/users');
        const json = await response.json();

        console.log(json);

        new Users(json);

    } catch (error) {

    }
}



/*
     const div = document.createElement('div');
     div.classList.add('grid');

     div.innerHTML = ..


*/

fetchAwait();
