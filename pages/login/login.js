const valid = {
    username: 'hello',
    password: '12345'
}

function login() {
    const username = document.getElementById('username');
    const password = document.getElementById('password');

    console.log(username.value, password.value);

    if (username.value === valid.username && password.value === valid.password) {
        window.location.href = '/home';
        success('you are valid');
        localStorage.setItem('user', username.value);
    } else {
        failed('you cannot login');
    }
}

const success = (text) => {
    showMsg(text, 'success');
}

const failed = (text) => {
    showMsg(text, 'failed');
}

const showMsg = (text, type) => {
    const msg = document.getElementById('msg');

    msg.setAttribute('class', type);

    msg.innerHTML = text;
}




document.getElementById('login').addEventListener('click', login);

